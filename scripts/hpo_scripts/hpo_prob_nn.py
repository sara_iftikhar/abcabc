"""
========================================================
Hyperparameter optimization of Probabilistic NN
========================================================
"""
import os
import sys
import site
proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))
site.addsitedir(proj_dir)

import random
import numpy as np
import tensorflow as tf
from SeqMetrics import RegressionMetrics

from ai4water.utils import TrainTestSplit
from ai4water.hyperopt import HyperOpt, Categorical, Real, Integer
from ai4water.utils.utils import dateandtime_now, jsonize, reset_seed

from utils import make_data, BayesModel

# %%

data, _, encoders= make_data(encoding='le')
TrainX, TestX, TrainY, TestY = TrainTestSplit(seed=142).\
    random_split_by_groups(x=data.iloc[:,0:-1], y=data.iloc[:, -1],
    groups=data['Adsorbent'])

input_features = TrainX.columns.tolist()
output_features = 'qe'

# %%
train_size = len(TrainX)

ITER = 0
VAL_SCORES = []
SUGGESTIONS = []
num_iterations = 12  # number of hyperparameter iterations
num_epochs = 10
SEP = os.sep
PREFIX = f"hpo_prob_nn_{dateandtime_now()}"  # folder name where to save the results
algorithm = "bayes"
uncertainty_type = "aleoteric"

# %%

param_space = [
    Categorical(["sigmoid", "relu", "elu", "tanh"], name="activation"),
    Integer(5, 32, name="hidden_units"),
    Real(1e-5, 0.005, name="learning_rate"),
    #Integer(2, 4, name="num_layers"),
    Categorical([8, 16, 24, 32, 40], name="batch_size")
]

# %%
x0 = ["sigmoid", 10, 0.001, 32]

# %%
def negative_loglikelihood(targets, estimated_distribution):
    return -estimated_distribution.log_prob(targets)

def objective_fn(
        return_model:bool = False,
        num_epochs = num_epochs,
        prefix=None,
        **suggestions
):
    global ITER

    suggestions = jsonize(suggestions)

    # num_lyrs = suggestions['num_layers']
    # hidden_units = [suggestions['hidden_units']] * num_lyrs

    _model = BayesModel(
        model={"layers": dict(
            hidden_units=[suggestions['hidden_units'], suggestions['hidden_units']],
            train_size=train_size,
            activation=suggestions['activation'],
            uncertainty_type=uncertainty_type,
        )},
        batch_size = suggestions['batch_size'],
        epochs = num_epochs,
        lr = suggestions['learning_rate'],
        input_features = input_features,
        output_features = output_features,
        category="DL",
        optimizer="RMSprop",
        loss = negative_loglikelihood,
        prefix=prefix or PREFIX,
        verbosity=0
    )

    reset_seed(142, os=os, tf=tf, random=random)

    if return_model:
        _model.fit(TrainX.values, TrainY.values,
                  validation_data=(TestX, TestY.values))

        mean = (_model._model(TestX)).mean()
        metrics = RegressionMetrics(TestY, mean.numpy())
        #_model.evaluate(TestX, TestY, metrics=["r2", "r2_score", 'rmse'])
        return _model, metrics

    _ = _model.fit(x=TrainX.astype(np.float32),
                  y=TrainY.astype(np.float32),
                  validation_data=(TestX.astype(np.float32),
                                   TestY.astype(np.float32)))

    y_dist = _model._model(TestX)

    mean = y_dist.mean()

    metrics = RegressionMetrics(TestY, mean.numpy())
    val_score_ = metrics.nse()

    if not np.isfinite(val_score_):
        val_score_ = -999

    val_score = 1 - val_score_

    VAL_SCORES.append(val_score)
    best_score = round(np.nanmin(VAL_SCORES).item(), 2)
    bst_iter = np.argmin(VAL_SCORES)

    ITER += 1

    #run.log({"nse": val_score_})
    #run.log({"r2": metrics.r2()})
    #run.log({"rmse": metrics.rmse()})

    print(f"{ITER} {round(val_score, 2)} {round(val_score_, 2)}. Best was {best_score} at {bst_iter} {suggestions}")

    return val_score

# %%

# optimizer = HyperOpt(
#     algorithm=algorithm,
#     objective_fn=objective_fn,
#     param_space=param_space,
#     x0=x0,
#     num_iterations=num_iterations,
#     process_results=False,  # we can turn it False if we want post-processing of results
#     opt_path=f"results{SEP}{PREFIX}"
# )

# %%

# res = optimizer.fit()
#
# # %%
# # print optimized hyperparameters
#
# print(optimizer.best_paras())

# %%

# # build and train the model with optimized hyperparameters
#
# model, metrics = objective_fn(prefix=f"{PREFIX}{SEP}best",
#                      return_model=True,
#                      epochs=num_epochs,
#                      verbosity=1,
#                      **optimizer.best_paras())

# %%

# print(f'Test scores: \nNSE:{metrics.nse()}\nR2: {metrics.r2()}\nRMSE: {metrics.rmse()}')
